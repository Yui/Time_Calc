﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Time_Calc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BTCalculate_Click(object sender, EventArgs e)
        {
            int seconds = 0, hours = 0, minutes = 0;
            int tmp = 0;
            foreach (string line in textBox1.Lines)
            {
                try
                {
                    if (!line.StartsWith("//"))
                    {
                        string[] x = line.Split(':');
                        seconds += Convert.ToInt16(x[2]);
                        minutes += Convert.ToInt16(x[1]);
                        hours += Convert.ToInt16(x[0]);
                    }                    
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message, "Error");
                }
            }
            //Calc
            //Seconds
            tmp = seconds / 60;
            seconds = seconds -  (tmp*60);
            minutes += tmp;
            //Minutes
            tmp = minutes / 60;
            minutes = minutes - (tmp * 60);
            hours += tmp;
            //Hours
            textBox2.Text = hours.ToString() + "H " + minutes.ToString() + "M " + seconds.ToString() + "s";
        }
    }
}
